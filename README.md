# ManTester parser

Write manual test cases in Markdown and generate JSON data structure.

---

Go to https://mantester.com/ to learn more.
