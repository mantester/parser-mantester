## `CD-001` Prev test case

### Prev steps

- Prev step 1.
- Prev step 2.


## `CD-002` Some _test case_ 2

Description 1 with text _emphasized_ and **bold**.

Some <span class="class1">explicit html element</span>.

!smoke

@domain1 @domain2 @domain3

Description 2 with [some link](mantester.com).

#### Lower-level heading in description

And some text within the heading.

### Steps

Description before steps.

- Test step 1 with _emphasized_ and **bold** text.
- Test step 2 with [another link](mantester.com).
- Test step 3 with sub list:
    - Sublist 3 1 with _emphasized_ text.
    - Sublist 3 2 with **bold** text.
        - Sublist 3 2 1
        - Sublist 3 2 2
    - Sublist 3 3.
- Test step 4 with ordered sub list:
    1. Sublist 4 1
    2. Sublist 4 2
       1. Sublist 4 2 1
       2. Sublist 4 2 2
       3. Sublist 4 2 3
    3. Sublist 4 3

Description after steps.

- Second list in steps.
- With more items.

Other description.

1. Ordered list steps 1.
2. Ordered list steps 2.
3. Ordered list steps 3.

Description after all steps.

#### Lower-level heading in steps

And some text here.

### Same-level heading after steps

Same-level heading description.

- Same-level step 1.
- Same-level step 2.
- Same-level step 3.

## `CD-003` Next test case

### Next steps

- Next step 1.
- Next step 2.
