## `CD-001`      Some test case 1

!smoke

@domain1 @domain2 @domain3

### Steps

- Test step 1 1.
- Test step 1 2.
- Test step 1 3.

## Some test case 3      `CD-003`

!core @domain3

### Steps

- Test step 3 1.
- Test step 3 2.
- Test step 3 3.

## Some      `CD-002`     test    case 2

Some @domain2 text !Enabling in @domain3 between.

### Steps

- Test step 2 1.
- Test step 2 2.
- Test step 2 3.
