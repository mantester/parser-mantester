import fs from 'fs'
import chalk from 'chalk'
import Diff from 'diff'

import { parse } from '../../src/index.js'

const encoding = 'utf8'

const match = /.*/

const replaceOnError = false
// const replaceOnError = true

let testCount = 0
let errorCount = 0
let skipCount = 0

const errors = []
const originalConsoleError = console.error
console.error = output => errors.push(output)

console.error = originalConsoleError

function processDir (dir, testedFn) {
  const files = fs.readdirSync(dir)
  for (let i = 0; i < files.length; i++) {
    const file = files[i]
    const isDir = fs.statSync(dir + file).isDirectory()
    if (isDir) {
      processDir(dir + file + '/', testedFn)
    } else if (endsWith(file, '.md')) {
      processFile(testedFn, dir + file)
      testCount++
    }
  }
}

function endsWith (str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1
}

function processFile (testedFn, markdownFile) {
  const fileBase = markdownFile.substr(0, markdownFile.length - 3)
  if (!match.test(fileBase)) {
    console.log(chalk.inverse(`Skipping non-matching test case ${fileBase}`))
    skipCount++
    return
  }
  console.log(chalk.inverse('Processing test case') + ` ${fileBase}`)
  const markdown = fs.readFileSync(markdownFile, encoding)
  const parsed = testedFn(markdown)
  const actualJson = JSON.stringify(parsed, null, 2) + '\n'
  const jsonFile = fileBase + '.json'
  const expectedJson = fs.readFileSync(jsonFile, encoding)
  let message
  if (expectedJson !== actualJson) {
    if (replaceOnError) {
      message = chalk.redBright(`Assertion failed for test case ${fileBase}. Replacing content.`)
      console.log(message)
      fs.writeFileSync(jsonFile, actualJson)
    } else {
      const diff = Diff.diffWordsWithSpace(expectedJson, actualJson)
      message = chalk.redBright(`Assertion failed for test case ${fileBase}:\n`)
      diff.forEach(function (part) {
        const color = part.added ? 'green' : part.removed ? 'red' : 'grey'
        message += chalk[color](part.value)
      })
      console.log(message)
    }
    errorCount++
  }
}

function run (testsDir, testedFn) {
  processDir(testsDir + '/', testedFn)

  let result = chalk.inverse(`Tests run: ${testCount}`)
  if (skipCount !== 0) {
    result += `, skipped: ${skipCount}`
  }
  if (errorCount !== 0) {
    result += ', ' + chalk.bgRedBright(`errors: ${errorCount}`)
  }
  const passCount = testCount - errorCount - skipCount
  if (passCount !== 0) {
    result += ', ' + chalk.bgGreenBright(`passed: ${passCount}`)
  }
  console.log(result)
}

run('test/parse', parse)
